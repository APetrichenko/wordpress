<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+G+r`.@J;?ZC}S>Ee}yr;A6{?7X^B)l<ky`_~n|8Ihw?UlCtsX]jN$L/!]nMDW/c');
define('SECURE_AUTH_KEY',  '> |8hT_FRZJ(c=DHZ^).fE,7{a#+{LKk?!ULBSUUC_Tj=t=Y_Fy5[2ZN@+<m;gnh');
define('LOGGED_IN_KEY',    'pfkYpq@e`@MVBNl>s89cEh$:R]Y|6rN{x4AR{x4Q.@~[:j46{-qY@Qg1$-0RN4?0');
define('NONCE_KEY',        '1[TCfQ|^nGKMB?|f)UHCJA=SLzU|19^+4_=Oh2bqZc`UP;!Vf[J4)VrUUd&2.q^O');
define('AUTH_SALT',        'lDs3Mh_J~}Gvk[TV6qE8A:hMVS?G:y(T<OLCAK&U>vM};,6+a54<&UuGU+V0+~$e');
define('SECURE_AUTH_SALT', '!m_b<H8nFW8Ey:yWXHW~(g;qRC<qSj>C-9<.WrM2emTn$7ZS+PL}$eku,^R4LI]I');
define('LOGGED_IN_SALT',   '7^T_,W6#towHh9Q4Xw5P!h.G)-b^~Qz*>k{W#OlEe179*;G2=QPCss[WWXd9y0.s');
define('NONCE_SALT',       'Me6+]RbJ_dqklEQUz=YQ5cy}QM}?S%dGM7+nD99u5>]Z`HM$;tR%!vjQFvX@V.?Z');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
