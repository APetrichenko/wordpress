<?php get_header(); ?>
<?php
//get_search_form();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		echo "<div>";
		$images = get_field('gallery');
		$size = 'full'; // (thumbnail, medium, large, full or custom size)

		if( $images ): ?>
    	<ul>
        <?php foreach( $images as $image ): ?>
            <li>
            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
            </li>
        <?php endforeach; ?>
    	</ul>
		<?php endif;
		$value = get_field( "owner_name" );
		if( $value ) {
 			echo $value;
		} else {
			echo 'empty';
    	}
		echo "<br>";
		$value = get_field( "owner_phone" );
		if( $value ) {
 			echo $value;
		} else {
			echo 'empty';
    	}
    	echo "<br>";
    	if( have_rows('car_properties') ):

 	// loop through the rows of data
    	while ( have_rows('car_properties') ) : the_row();

			the_sub_field('prop_name');
			echo " - ";
        	the_sub_field('prop_value');
        	echo "<br>";

    	endwhile;

		else :

    	echo "no rows found";

		endif;

		echo "</div>";
		/*$value = get_field( "owner_name" );

		if( $value ) {
    
    	echo $value;

		} else {

    	echo 'empty';
    
}*/
		
		// Post Content here
		//
	} // end while
} // end if
?>
<?php get_footer(); ?>
