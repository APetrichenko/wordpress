<?php get_header(); ?>
<h1>search</h1>
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		echo "<div>";
		the_post();
		the_title();
		the_content();
		echo "<br>";
		the_date();
		echo "<br>";
		the_author_meta('user_login');
		echo "</div>";
		// Post Content here
		//
	} // end while
} // end if
?>
<?php get_footer(); ?>