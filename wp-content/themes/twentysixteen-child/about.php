<?php
/*
Template Name: About
*/
?>
<?php get_header(); ?>
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		the_title();
		the_content();
		echo do_shortcode( '[contact-form-7 id="8" title="Contact form 1"]' );
		the_date();
		echo "<br>";
		the_author_meta('user_login');
		
		// Post Content here
		//
	} // end while
} // end if
?>
<?php get_footer(); ?>