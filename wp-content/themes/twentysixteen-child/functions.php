<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/css/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function create_post_type() { // создаем новый тип записи
register_post_type( 'cars', // указываем названия типа
array(
'labels' => array(
'name' => __( 'Car' ), // даем названия разделу для панели управления
'singular_name' => __( 'Car' ), // даем названия одной записи
'add_new' => __('Добавить новый'),// далее полная русификация админ. панели
'add_new_item' => __('Добавить новый car'),
'edit_item' => __('Редактировать car'),
'new_item' => __('Новый car'),
'all_items' => __('Все car'),
'view_item' => __('Просмотр car'),
'search_items' => __('Поиск car'),
'not_found' => __('Нет car'),
'not_found_in_trash' => __('car не найдены'),
'menu_name' => 'Cars'
),
'public' => true,
'menu_position' => 5, // указываем место в левой баковой панели
//'rewrite' => array('slug' => 'cars'), // указываем slug для ссылок например: http://mysite/reviews/
'supports' => array('title', 'editor', 'thumbnail', 'excerpt','comments'), // тут мы активируем поддержку миниатюр
'has_archive' => true
)
);
}

add_action( 'init', 'create_post_type' ); 


 
// инициируем добавления типа
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
 
function create_topics_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => __( 'Brands', 'taxonomy general name' ),
    'singular_name' => __( 'Brand', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Brand' ),
    'all_items' => __( 'All Brand' ),
    'parent_item' => __( 'Parent Brand' ),
    'parent_item_colon' => __( 'Parent Brand:' ),
    'edit_item' => __( 'Edit Brand' ), 
    'update_item' => __( 'Update Brand' ),
    'add_new_item' => __( 'Add New Brand' ),
    'new_item_name' => __( 'New Brand Name' ),
    'menu_name' => __( 'Brands' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy('brands',array('cars'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'brand' ),
  ));
 
}
add_action( 'init', 'create_topics_nonhierarchical_taxonomy', 0 );
 
function create_topics_nonhierarchical_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Colors', 'taxonomy general name' ),
    'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Topics' ),
    'popular_items' => __( 'Popular Topics' ),
    'all_items' => __( 'All Topics' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Color' ), 
    'update_item' => __( 'Update Color' ),
    'add_new_item' => __( 'Add New Color' ),
    'new_item_name' => __( 'New Color Name' ),
    'separate_items_with_commas' => __( 'Separate Colors with commas' ),
    'add_or_remove_items' => __( 'Add or remove Colors' ),
    'choose_from_most_used' => __( 'Choose from the most used Colors' ),
    'menu_name' => __( 'Colors' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('colors','cars',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'color' ),
  ));
}
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

?>