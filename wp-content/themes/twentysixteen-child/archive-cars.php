<?php get_header(); ?>
<?php
get_search_form();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		echo "<div>";
		
		echo "<h2>";the_title();echo "</h2>";
		the_content();
		echo "<br>";
		echo "<i>";the_time('j F Y');echo "</i>";
		echo "<br>";
		echo "<b>";the_author_meta('user_login');echo "</b>";
		echo "</div>";
		
		// Post Content here
		//
	} // end while
} // end if
?>
<?php get_footer(); ?>