<?php get_header(); ?>
<?php
get_search_form();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		echo "<div>";
		
		the_title();
		the_content();
		echo "<br>";
		the_date();
		echo "<br>";
		the_author_meta('user_login');
		echo "</div>";
		
		// Post Content here
		//
	} // end while
} // end if
?>
<?php get_footer(); ?>