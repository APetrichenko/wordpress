
<?php get_header(); ?>
</header>
<div id="main-container">
		<div class="container">
			<div class="row">
				<div style="font-size:16px" class="breadcrumbs-top">
					<a href="#">Home</a>&nbsp;≫&nbsp;
					<a href="javascript:window.history.back();">Search</a>&nbsp;≫&nbsp;<?php the_title();?>
				</div>
				<div class="single-product__header">
					<div class="row" id="single-header-booknow">
						<div class="col-md-7">
							<h1><?php the_title();?></h1>
						</div>				
					</div>
				</div>
				
				<div class="row" id="main-stuff">
					<?php
						$images = get_field('gallery');
						$size = 'full';
						if( $images ) :
							?>
							<div class="col-md-7">
								<div class="js-single-product-slider single-product__slider owl-theme" style="opacity: 1; display: block;">
									<!-- <div class="owl-wrapper-outer">
										<div class="owl-wrapper"> -->
											<?php foreach( $images as $image ): ?>
												<!-- <div class="owl-item" style="width: 606px;"> -->
													<div class="item">
														<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
													</div>
												<!-- </div> -->
											<?php endforeach; ?>
									<!-- 	</div>
																		</div> -->
									<!-- <div class="owl-controls clickable">
										<div class="owl-pagination">
											<div class="owl-page"><span class=""></span></div>
											<div class="owl-page"><span class=""></span></div>
											<div class="owl-page active"><span class=""></span></div>
										</div>
										<div class="owl-buttons">
											<div class="owl-prev">prev</div>
											<div class="owl-next">next</div>
										</div>
									</div> -->
								</div>
							</div>
					<?php endif; ?>
					<div class="col-md-5">
						<h2>
							<strong class="price-range">Price Range:</strong>
							$200 - $250 per hour
						</h2>
						<?php
							$size = get_field ( 'size' );
							if ( $size ) :
								?>
								<div class="single-product__content-style">
									<span class="single-product__content-style-label">Vessel Size:</span>
									<span class="single-product__content-style-val"> <?php echo $size; ?> feet</span>
								</div>
						<?php endif; ?>
						<?php
							$speed = get_field ( 'speed' );
							if ( $speed ) :
								?>
								<div class="single-product__content-style">
									<span class="single-product__content-style-label">Vessel Speed:</span>
									<span class="single-product__content-style-val"><?php echo $speed; ?> knots +</span>
								</div>
						<?php endif; ?>
						<?php
							$occupancy = get_field ( 'occupancy' );
							if ( $occupancy ) :
								?>
								<div class="single-product__content-style">
									<span class="single-product__content-style-label">Vessel Occupancy:</span>
									<span class="single-product__content-style-val"><?php echo $occupancy; ?> people</span>
								</div>
							<?php endif; ?>
						<?php
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
									$terms = get_the_terms( $post->ID,'boatfeatures' );
									if ( $terms ) :
										?>
										<div class="single-product__content-style _divided">
											<span class="single-product__content-label">Vessel Features:</span>
										</div>
										<div class="single-product__checked-wrap-wide">
									
											<?php foreach( $terms as $term ) : ?>
												<span class="single-product__checked"><?php echo $term->name;?></span>
											<?php endforeach;?>
										</div>
									<?php endif; ?>
									<?php
								}
							}
									?>
							
						</div>
				</div>
				
				<?php
					$type_of_trip = get_field( 'type_of_trip' );
					if ( $type_of_trip ) : 
									?>
					<div class="row single-product__about">
						<div class="col-md-3">
							<h3>Type of trip</h3>
						</div>
						<div class="single-product__checked-wrap-wide">
							<?php if( in_array('private_charter', $type_of_trip ) ):?>
								<span class="single-product__checked"><?php echo "Private Charter"; ?></span>
							<?php endif; ?>
							<?php if( in_array('group_trips', $type_of_trip ) ):?>
								<span class="single-product__checked"><?php echo "Group Trips"; ?></span>
							<?php endif; ?>
							<?php if( in_array('party_board_fishing', $type_of_trip ) ):?>
								<span class="single-product__checked"><?php echo "Party Board Fishing"; ?></span>
							<?php endif; ?>	
						</div>
					</div>
				<?php endif; ?>
					
				<?php if( have_rows('content') ): ?>
						<div class="row single-product__about">
							<?php while ( have_rows('content') ) : the_row(); ?>
								<div class="col-md-3">
									<h3><?php the_sub_field('title'); ?></h3>
								</div>
								<div class="col-md-9">
									<p><?php the_sub_field('text'); ?></p>
								</div>
							<?php endwhile; ?>
						</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>