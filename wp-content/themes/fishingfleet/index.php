	
	<?php get_header(); ?>
	<?php get_search_form(); ?>
	</header>
	<div id="main-container">
		<div class="container">
			<section class="row discount">
			<?php
				$args = array(
					'category_name' => 'discounts' 
				);
				$query = new WP_Query( $args );	
				if ( $query->have_posts() ) {
					$category = get_the_category();
					?>
				<div class="col-md-12 ">
					<h2><?php echo $category[0]->cat_name; ?></h2>
				</div>
				
				<?php while ( $query->have_posts() ) { ?>
				<div class="col-md-4 col-sm-4">
					<div class="discount-card">
						<div class="figure">
							<?php
								/*$category = get_the_category(); 
								echo $category[0]->cat_name;*/
								$query->the_post();
								$image = get_field('image');
								$size = 'category-offers';
								if ( $image ) {
            						echo wp_get_attachment_image( $image, $size );
            					}
        					 ?>
							<!-- <img src="http://lorempixel.com/320/115/"> -->
						</div>
						<!-- <div class="mark"><?php// echo get_field('discount'); ?>% off</div> -->
						<p><?php the_content(); ?></p>
					</div>
				</div>	
			<?php
					}
				}
				wp_reset_postdata();
				
			?>
			</section>	
			<section class="row recommence">
				<div class="col-md-12">
					<h1 style="text-align: center;">Lorem Ipsum is simply dummy text of the the printing</h1>
					<p style="text-align: center;">
						Lorem Ipsum is simply dummy text of the the printing
					</p>
				</div>	
				<?php
				$args = array(
					'post_type' => 'boats',
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();?>
						<div class="col-md-4 col-sm-6">						
							<div class="recommence-card">
								<div class="recommence-card__content">
									<div class="recommence-card__mark">
									<?php
										$cur_terms = get_the_terms( $post->ID, 'boatlabels' );
										foreach( $cur_terms as $cur_ter ){
											if ( $cur_ter->name == "Recommended" )
												echo "recommended";
										}
									?>
									</div>
									<?php 
										$images = get_field('gallery');
										$size = 'lodka_image';
										if( $images ) :
											echo wp_get_attachment_image( $images[0]['ID'], $size );
										endif;	
        					 		?>
		        					<div class="recommence-card__description">
										<div class="title"><?php the_title();?></div>
										<?php
											$price = get_field ( 'price' );
											if ( $price ) :
												?>
										<p class="price"> from <span>$<?php echo $price; ?></span> per hour </p>
										<?php endif; ?>
										<div class="options">
											<i class="icon-yacht"></i>
											<?php
											$size = get_field ( 'size' );
											if ( $size ) :
												?>
											 <span><?php echo $size; ?> feet</span>
											 <?php endif; ?>
										</div>
										<div class="options">
											<i class="icon-person"></i>
											<?php
											$occupancy = get_field ( 'occupancy' );
											if ( $occupancy ) :
												?>
											<span><?php echo $occupancy; ?> people</span>
											<?php endif; ?>
										</div>
									</div>
								</div>				
							</div>
						</div>
			<?php
					}
				}
				else {
					echo "Do not search post";
				}
			?>
			</section>
		</div>
	</div>
<?php get_footer(); ?>