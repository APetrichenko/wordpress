<div class="search-banner-layer">
			<form>
				<input type="hidden" name="post_type" value="boats" />
				<div class="container banner-content-area">
					<div class="heading-block">
						<h1>Lorem Ipsum is simply dummy text of the printing</h1>
						<h2>Lorem Ipsum is simply dummy text of the printing</h2>
					</div>
					<div class="category-checkboxes-block">
						<div class="squaredTwo">
							<input type="checkbox" value="1" id="private_charter" name="private_charter" <?php if ( isset($_GET['private_charter']) ) echo "checked"; ?> />
							<label for="private_charter">Private Charter</label>
						</div>
						<div class="squaredTwo">
							<input type="checkbox" value="1" id="group_trips" name="group_trips" <?php if ( isset($_GET['group_trips']) ) echo "checked"; ?> />
							<label for="group_trips">Group Trips</label>
						</div>
						<div class="squaredTwo">
							<input type="checkbox" value="1" id="party_board_fishing" name="party_board_fishing" <?php if ( isset($_GET['party_board_fishing']) ) echo "checked"; ?> />
							<label for="party_board_fishing">Party Board Fishing</label>
						</div>
					</div>
					<div class="selects-area row">
						<div class="col-sm-3">
							<div class="castom-select">
								<select class="js-select-2" name="term_boattypes">
									<option>All</option>
									<?php
										$terms = get_terms( 'boattypes' );
										if( $terms && ! is_wp_error($terms) ){
											foreach( $terms as $term ){
												$term_slug = $term->slug;
												?>
												<option value= <?php echo $term->slug;
												if ( isset( $_GET['term_boattypes'] ) ) {
													selected($_GET['term_boattypes'], $term_slug );
												}
												 ?> > <?php echo $term->name; ?> </option>
												<?php
											}
										}
									?>
								</select>
								
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="castom-select">
								<select class="js-select-2" multiple="true" name="term_boatfeatures[]">
									<option disabled>Boat Features</option>
									<?php
										$terms = get_terms( 'boatfeatures' );
										if( $terms && ! is_wp_error($terms) ){
											foreach( $terms as $term ){
												$term_slug = $term->slug;
												?>
												<option value= <?php echo $term->slug;
												if (isset($_GET['term_boatfeatures'])){
													$key = array_search($term_slug, $_GET['term_boatfeatures']);
													selected($_GET['term_boatfeatures'][$key], $term_slug );
												}
												 ?> > <?php echo $term->name; ?> </option>
												<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="castom-select">
								<select class="js-select-2" multiple="true" name="price">
									<option>Price</option>
									<option>$150-165 per hour</option>
									<option>$170-175 per hour</option>
									<option>$180-185 per hour</option>
									<option>$190-195 per hour</option>
									<option>$200 + per hour</option>
								</select>
							</div>
						</div>				
						
						<div class="col-sm-3">
							<input type="submit" class="wpm-btn btn" value="Search">Search</button>
						</div>
					</div>
				</div>
			</form>
		</div>