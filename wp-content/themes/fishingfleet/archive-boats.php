<?php get_header(); ?>
<?php get_search_form(); ?>
<div id="main-container">
		<div class="container">
				<section class="row recommence">
					<?php
					/*$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$params = array(
						'posts_per_page' => 2, // количество постов на странице
						'post_type'       => 'boats', // тип постов
						'paged'           => $current_page // текущая страница
					);
					query_posts($params);
					$wp_query->is_archive = true;
					$wp_query->is_home = false;*/
					if ( isset( $_GET[ 'term_boattypes' ]) || isset( $_GET[ 'term_boatfeatures' ]) || isset( $_GET[ 'private_charter' ]) || isset( $_GET[ 'group_trips' ]) || isset( $_GET[ 'party_board_fishing' ]) ) {
						echo "<h2> Поиск выполнен по следующим параметрам: <br>";
						if (isset( $_GET[ 'term_boattypes' ])) {
							$cur_terms = get_terms( 'boattypes' );
							foreach( $cur_terms as $cur_term ){
								if ( $cur_term->slug == $_GET[ 'term_boattypes' ] )
									echo "Boat Types: ". $cur_term->name;
							}
							echo "<br>";
						}
						if (isset( $_GET[ 'term_boatfeatures' ])) {
							echo "Boat Features: ";
							$cur_terms = get_terms( 'boatfeatures' );
							foreach( $cur_terms as $cur_term ){
								for ( $i=0; $i < count($_GET[ 'term_boatfeatures' ]); $i++ ) {
									if ($cur_term->slug == $_GET[ 'term_boatfeatures' ][$i]) {
										if ( count( $_GET[ 'term_boatfeatures' ] ) == 1 ) {
											echo $cur_term->name;
										} elseif( $i+1 != count($_GET[ 'term_boatfeatures' ]) ){
											echo $cur_term->name . ', ';
										} else {
											echo $cur_term->name;
										}
									}
								}
							}
							echo "<br>";
						}
						if (isset( $_GET[ 'private_charter' ])) {
							echo "Private Charter";
							 if (isset( $_GET[ 'party_board_fishing' ]) || isset( $_GET[ 'group_trips' ]) )
							 	echo ", ";
						}
						if (isset( $_GET[ 'group_trips' ])) {
							echo "Group Trips";
							 if (isset( $_GET[ 'party_board_fishing' ]) )
							 	echo ", ";
						}
						if (isset( $_GET[ 'party_board_fishing' ])) {
							echo "Party Board Fishing";
						}
						echo "<br>";
					}
					if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();?>
						<div class="col-md-4 col-sm-6">						
							<div class="recommence-card">
								<div class="recommence-card__content">
									<div class="recommence-card__mark">
									<?php
										$cur_terms = get_the_terms( $post->ID, 'boatlabels' );
										foreach( $cur_terms as $cur_term ){
											if ( $cur_term->name == "Recommended" )
												echo "recommended";
										}
									?>
									</div>
									<?php 
										$images = get_field('gallery');
										$size = 'lodka_image';
										if( $images ) :
											echo wp_get_attachment_image( $images[0]['ID'], $size );
										endif;	
        					 		?>
		        					<div class="recommence-card__description">
										<div class="title"><?php the_title();?></div>
										<?php
											$price = get_field ( 'price' );
											if ( $price ) :
												?>
										<p class="price"> from <span>$<?php echo $price; ?></span> per hour </p>
										<?php endif; ?>
										<div class="options">
											<i class="icon-yacht"></i>
											<?php
											$size = get_field ( 'size' );
											if ( $size ) :
												?>
											 <span><?php echo $size; ?> feet</span>
											 <?php endif; ?>
										</div>
										<div class="options">
											<i class="icon-person"></i>
											<?php
											$occupancy = get_field ( 'occupancy' );
											if ( $occupancy ) :
												?>
											<span><?php echo $occupancy; ?> people</span>
											<?php endif; ?>
										</div>
									</div>
								</div>				
							</div>

						</div>
			<?php
					}
					//the_posts_pagination();
				}

				else {
					echo "<h3>Do not search post<h3>";
				}
			?>
			</section>
		</div>
	</div>
<?php get_footer(); ?>