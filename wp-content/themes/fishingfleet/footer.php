<div class="widgets-area" style="background-color:<?php the_field('color','option');?>">
		<div class="sidebar footer_side container">
			<div class="row">
			<div id="wpm_help_info-2" class="col-md-12 side widget widget_wpm_help_info">
				<div class="help_block">
					<span class="help-text"><?php the_field('text','option');?></span>
					<img src="img/r-arrow.png" alt="arrow">
					<div>
						<a href="tel:850-837-1995" class="help-phone"><?php the_field('phone','option');?></a><br>
						<a href="tel:850-837-1995" class="help-phone2"><?php the_field('phone2','option');?></a>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">				
					<p class="rights"><?php the_field('copyright','option');?></p>				
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer();?>
</body>
</html>