
<?php get_header(); ?>

<div id="main-container">
		<div class="container">
				<section class="row recommence">
					<?php
					if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();?>
						<div class="col-md-4 col-sm-6">						
							<div class="recommence-card">
								<div class="recommence-card__content">
									<div class="recommence-card__mark">
									<?php
										$cur_terms = get_the_terms( $post->ID, 'boatlabels' );
										foreach( $cur_terms as $cur_term ){
											if ( $cur_term->name == "Recommended" )
												echo "recommended";
										}
									?>
									</div>
									<?php 
										$images = get_field('gallery');
										$size = 'lodka_image';
										if( $images ) :
											echo wp_get_attachment_image( $images[0]['ID'], $size );
										endif;	
        					 		?>
		        					<div class="recommence-card__description">
										<div class="title"><?php the_title();?></div>
										<?php
											$price = get_field ( 'price' );
											if ( $price ) :
												?>
										<p class="price"> from <span>$<?php echo $price; ?></span> per hour </p>
										<?php endif; ?>
										<div class="options">
											<i class="icon-yacht"></i>
											<?php
											$size = get_field ( 'size' );
											if ( $size ) :
												?>
											 <span><?php echo $size; ?> feet</span>
											 <?php endif; ?>
										</div>
										<div class="options">
											<i class="icon-person"></i>
											<?php
											$occupancy = get_field ( 'occupancy' );
											if ( $occupancy ) :
												?>
											<span><?php echo $occupancy; ?> people</span>
											<?php endif; 
											
											?>
										</div>
									</div>
								</div>				
							</div>
						</div>
			<?php
					}
				}

				else {
					echo "Do not search post";
				}
			?>
			</section>
		</div>
	</div>
<?php get_footer(); ?>