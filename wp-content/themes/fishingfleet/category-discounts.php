<?php get_header(); ?>
<section class="row discount">

<?php
if (have_posts()) :?>
	<div class="col-md-12 ">
		<h2><?php single_term_title(); ?></h2>
	</div>	
 <?php while ( have_posts() ) : 
	the_post();
?>	
				<div class="col-md-4 col-sm-4">
					<div class="discount-card">
						<div class="figure">
							<?php 
								$image = get_field('image');
								$size = 'full';
								if ( $image )
            						echo wp_get_attachment_image( $image, $size );
        					 ?>
							<!-- <img src="http://lorempixel.com/320/115/"> -->
						</div>
						<div class="mark"><?php echo get_field('discount'); ?>% off</div>
						<p><?php the_content(); ?></p>
					</div>
				</div>
<?php endwhile;
endif; 
?>
</section>

	 