<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>Demo Site</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body>
	<header>
		<div class="header-layout">
			<div class="color-line" style="background-color: #30b58c">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div class="logo">
								<a href="#">
									<?php
										$logo = get_field('logo','option');
										$size = 'full';
	            						echo wp_get_attachment_image( $logo, $size );
        					 		?>
								</a>
							</div>
						</div>
						
						<div id="phoneholder" class="col-md-3 col-sm-3" style="padding-right:0px">
							<ul class="header-items" style="padding-top: 12px; margin-left:28px;">
								<li class="phone-holder" style="width:100%">
									<p><?php the_field('teltext','option');?></p>
								</li>
								<li style="margin:0px; width:100%">
									<a style="text-align: center;" href="tel:850-837-1995"><?php the_field('phone','option');?></a>
								</li>
								<li style="margin:0px;width:100%">
									<a style="text-align: center;" href="tel:8778371981"><?php the_field('phone2','option');?></a>
								</li>
							</ul>
						</div>
						<?php
						wp_nav_menu( array(
							'theme_location'  => '',
							'menu'            => 'menu', 
							'container'       => 'div', 
							'container_class' => 'col-md-4 col-sm-3', 
							'menu_class'      => 'header-items', 
							
						) );
						?>
						<!-- <div class="col-md-4 col-sm-3">
							<ul class="header-items" style="color: #ffffff">							
								<li class="contact-holder">
									<a href="#" style="color: rgb(255, 255, 255); cursor: pointer;" class="popmake-62 pum-trigger">Contuct us</a>
								</li>
								<li class="social-holder">
									Follow Us <a href="#" target="_blank"><img src="/wp-content/themes/fishingfleet/img/fb.png" alt="Follow Us"></a>
								</li>
							</ul>
						</div> -->
						<div class="col-md-2 col-sm-3" style="text-align:center;">
							<?php $rightimage = get_field('rightimage','option');
									$size = 'medium';
            						echo wp_get_attachment_image( $rightimage, $size,$icon = false, array('style'=>'height: 112px; padding:5px;') );
        					 		?>
							<!-- <img src="/wp-content/themes/fishingfleet/img/thumbnail_BestinDestin2017-002.jpg" style="height: 112px; padding:5px;"> -->
						</div>
					</div>
				</div>
			</div>
		</div>	