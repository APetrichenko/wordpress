<?php
function my_theme_enqueue_styles() {
	wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.min.css' );
	wp_enqueue_style( 'vendors', get_template_directory_uri() . '/css/vendors.min.css' );
	
	wp_enqueue_script('vendorsjs', get_template_directory_uri() . '/js/vendors.min.js');
	wp_enqueue_script('typeahead', get_template_directory_uri() . '/js/typeahead.js');
	wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js');
	wp_enqueue_script('mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel.min.js');
	wp_enqueue_script('mCustomScrollbar', get_template_directory_uri() . '/js/jquery.mCustomScrollbar.min.js');
	wp_enqueue_script('rangeSlider', get_template_directory_uri() . '/js/ion.rangeSlider.min.js');
	wp_enqueue_script('mainjs', get_template_directory_uri() . '/js/main.min.js');
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function create_post_type() { // создаем новый тип записи
	register_post_type( 'boats', array(
		'labels' => array(
		'name' => __( 'Boat' ), // даем названия разделу для панели управления
		'singular_name' => __( 'Boat' ), // даем названия одной записи
		'add_new' => __('Add new'),// далее полная русификация админ. панели
		'add_new_item' => __('Add new boat'),
		'edit_item' => __('Edit boat'),
		'new_item' => __('New boat'),
		'all_items' => __('All boat'),
		'view_item' => __('Show boat'),
		'search_items' => __('Search boat'),
		'not_found' => __('Do not boat'),
		'not_found_in_trash' => __('Boat do not search'),
		'menu_name' => 'Boats'
		),
		'public' => true,
		'menu_position' => 5, // указываем место в левой баковой панели
		//'rewrite' => array('slug' => 'cars'), // указываем slug для ссылок например: http://mysite/reviews/
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt','comments'), // тут мы активируем поддержку миниатюр
		'has_archive' => true,
	) );	
}

add_action( 'init', 'create_post_type' );

if( function_exists( 'acf_add_options_page' ) ) {
	
	acf_add_options_page();
	
}

add_theme_support( 'menus' );

 
function create_boat_taxonomy() {
	
	$labels = array(
		'name' => __( 'Boat Types', 'taxonomy general name' ),
		'singular_name' => __( 'Boat Type', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Boat Type' ),
		'all_items' => __( 'All Boat Types' ),
		'parent_item' => __( 'Parent Boat Type' ),
		'parent_item_colon' => __( 'Parent Boat Type:' ),
		'edit_item' => __( 'Edit Boat Type' ), 
		'update_item' => __( 'Update Boat Type' ),
		'add_new_item' => __( 'Add New Boat Type' ),
		'new_item_name' => __( 'New Boat Type Name' ),
		'menu_name' => __( 'Boat Types' ),
	);    
	
	register_taxonomy( 'boattypes', array('boats'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'boattypes' ),
	));

	$labels = array(
		'name' => _x( 'Boat Features', 'taxonomy general name' ),
		'singular_name' => _x( 'Boat Feature', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Boat Features' ),
		'popular_items' => __( 'Popular Boat Features' ),
		'all_items' => __( 'All Boat Features' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Boat Feature' ), 
		'update_item' => __( 'Update Boat Feature' ),
		'add_new_item' => __( 'Add New Boat Feature' ),
		'new_item_name' => __( 'New Boat Feature Name' ),
		'separate_items_with_commas' => __( 'Separate Boat Feature with commas' ),
		'add_or_remove_items' => __( 'Add or remove Boat Features' ),
		'choose_from_most_used' => __( 'Choose from the most used Boat Features' ),
		'menu_name' => __( 'Boat Features' ),
	);    
	
	register_taxonomy( 'boatfeatures', 'boats', array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'boatfeatures' ),
	));
 
 $labels = array(
		'name' => _x( 'Boat Labels', 'taxonomy general name' ),
		'singular_name' => _x( 'Boat Label', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Boat Labels' ),
		'all_items' => __( 'All Boat Labels' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Boat Label' ), 
		'update_item' => __( 'Update Boat Label' ),
		'add_new_item' => __( 'Add New Boat Label' ),
		'new_item_name' => __( 'New Boat Label Name' ),
		'separate_items_with_commas' => __( 'Separate Boat Label with commas' ),
		'add_or_remove_items' => __( 'Add or remove Boat Labels' ),
		'choose_from_most_used' => __( 'Choose from the most used Boat Labels' ),
		'menu_name' => __( 'Boat Labels' ),
	);    
	
	register_taxonomy( 'boatlabels', 'boats' , array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'boatlabels' ),
	));
}
add_action( 'init', 'create_boat_taxonomy',0);

add_theme_support( 'post-thumbnails' );

add_image_size( 'category-offers', 600, 150 );
add_image_size( 'lodka_image', 320, 320 );

function get_query_posts( $query ){
	if ( ! is_admin() && $query->is_main_query() ) {
		//if ( $query->is_archive() ){
		if ($query->is_post_type_archive('boats')){
			
			$private_charter = '';
			$group_trips = '';
			$party_board_fishing = '';
			$term_boatfeatures = '';
			$term_boattypes = '';
			$query->set( 'post_type', 'boats');
			if (isset($_GET['term_boatfeatures'])) {
				$term_boatfeatures = array(
					'taxonomy' => 'boatfeatures',
					'field'    => 'slug',
					'terms'    => $_GET['term_boatfeatures'],
					'operator' => 'AND',
				);
			}
			if( $_GET['term_boattypes'] == "All" ) {
				$term_boattypes = array(
					'taxonomy' => 'boattypes',
					'field'    => 'slug',
					'terms'    => array('cruisers', 'fishing', 'motor_yachts' ),
				);
			} elseif (isset($_GET['term_boattypes'])) {
				$term_boattypes = array(
					'taxonomy' => 'boattypes',
					'field'    => 'slug',
					'terms'    => $_GET['term_boattypes'],
				);
			} 
			$query->set( 'tax_query', array(
				'relation' => 'AND',
				$term_boattypes,
				$term_boatfeatures,
			 ));
			if ( isset($_GET['private_charter']) )
				$private_charter = array(
					'key'	 	=> 'private_charter',
					'value'	  	=> '1',
					'compare' 	=> '==',
				);
			if ( isset($_GET['group_trips']) )
				$group_trips = array(
					'key'	 	=> 'group_trips',
					'value'	  	=> '1',
					'compare' 	=> '==',
				);
			if ( isset($_GET['party_board_fishing']) )
				$party_board_fishing = array(
					'key'	 	=> 'party_board_fishing',
					'value'	  	=> '1',
					'compare' 	=> '==',
				);
			$meta_query = array(
					'relation' => 'AND',
					$private_charter, $group_trips, $party_board_fishing);
			$query->set( 'meta_query', $meta_query );
			
				
		//}
	}
	}
}
add_action('pre_get_posts', 'get_query_posts');
?>